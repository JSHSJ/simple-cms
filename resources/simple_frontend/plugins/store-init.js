export default async context => {
  await context.store.dispatch('projects/GET_PROJECTS', context)
  await context.store.dispatch('positions/GET_POSITIONS', context)
  await context.store.dispatch('photos/GET_PHOTOS', context)
}
