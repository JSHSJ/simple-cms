package projects

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type PositionData struct {
	Title string `json:"title"`
}

func ListPositions(config ProjectConfig) ([]PositionData, error) {
	allPositions := []PositionData{}

	if _, err := os.Stat(config.PositionsFile); os.IsNotExist(err) {
		return allPositions, nil
	}

	file, err := ioutil.ReadFile(config.PositionsFile)

	if err != nil {
		return allPositions, &ProjectError{Message: "Can't read file: " + config.PositionsFile}
	}

	err = json.Unmarshal(file, &allPositions)

	if err != nil {
		return allPositions, &ProjectError{Message: "Can't parse json: " + config.PositionsFile}
	}

	return allPositions, nil
}
