package main

import (
	"gitlab.com/jshsj/simple_cms/internal/pkg/projects"
	"gitlab.com/jshsj/simple_cms/internal/pkg/remote"
	"gitlab.com/jshsj/simple_cms/internal/pkg/photos"
	"gitlab.com/jshsj/simple_cms/server"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"path/filepath"
)

type Config struct {
	LocalDirectory string
	RemoteURL      string
	Username       string
	Password       string
	PipelineURL    string
	PipelineToken  string
	PipelineRef    string
	FTPHost        string
	FTPUsername    string
	FTPPassword    string
	FTPPath        string
	PhotoUrl       string
}

func main() {
	config := Config{}

	file, err := ioutil.ReadFile("config/config.yaml")

	if err != nil {
		log.Fatal("Can't read config file.")
	}

	err = yaml.Unmarshal(file, &config)

	if err != nil {
		log.Fatal("Can't load config file.")
	}

	dataDir := config.LocalDirectory

	gitConfig := remote.GitConfig{
		LocalDirectory: config.LocalDirectory,
		RemoteURL:      config.RemoteURL,
		Username:       config.Username,
		Password:       config.Password,
		PipelineURL:    config.PipelineURL,
		PipelineToken:  config.PipelineToken,
		PipelineRef:    config.PipelineRef,
	}

	photoConfig := photos.PhotoConfig{
		Host:       config.FTPHost,
		Username:   config.FTPUsername,
		Password:   config.FTPPassword,
		RemotePath: config.FTPPath,
		PhotoUrl:   config.PhotoUrl,
		PhotoDirectory: filepath.Join(config.LocalDirectory, "/photos"),
	}

	projectConfig := projects.ProjectConfig{
		ProjectDirectory: filepath.Join(config.LocalDirectory, "/projects"),
		PositionsFile:    filepath.Join(config.LocalDirectory, "/positions.json"),
		ProjectsFile:     filepath.Join(config.LocalDirectory, "/projects.json"),
	}


	err = remote.SetupProjects(gitConfig)

	if err != nil {
		err = remote.FetchProjects(gitConfig)
	}

	if err != nil {
		log.Fatal(err)
	}

	//go browser.OpenURL("http://localhost:8080")

	server.StartServer(dataDir, projectConfig, gitConfig, photoConfig)
}
