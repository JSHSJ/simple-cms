package photos

import (
	"bytes"
	"github.com/gosimple/slug"
    "github.com/disintegration/imaging"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"time"
	"encoding/json"
	"os"
)


type PhotoConfig struct {
	Host string
	Username string
	Password string
	RemotePath string
	PhotoUrl string
	PhotoDirectory string
}


type Photo struct {
	Name string `json:"name"`
	Full PhotoSize `json:"full"`
	Thumbnail PhotoSize `json:"thumbnail"`
	Date time.Time `json:"date"`
}

type PhotoSize struct {
	Link string `json:"link"`
	Width int `json:"width"`
	Height int `json:"height"`
}

func createPhoto(file TempFile, config PhotoConfig) (string, *bytes.Buffer, *bytes.Buffer) {
	img, err := imaging.Decode(file.Data, imaging.AutoOrientation(true))
		if err != nil {
			log.Fatal(err)
		}

		m := imaging.Resize(img, 1024,0, imaging.Lanczos)
		newName := adjustFilename(file.Filename)

		thumb := imaging.Resize(img, 360, 0, imaging.Lanczos)


		buf := new(bytes.Buffer)

		err = imaging.Encode(buf, m, imaging.JPEG, imaging.JPEGQuality(85))

		if err != nil {
			log.Fatal(err)
		}

		if err != nil {
			log.Fatal(err)
		}

		// save thumbnail
		thumbBuf := new(bytes.Buffer)
		err = imaging.Encode(thumbBuf, thumb, imaging.JPEG, imaging.JPEGQuality(85))


		if err != nil {
			log.Fatal(err)
		}
		photo := Photo{
			Name: newName,
			Full: PhotoSize{
				Link: config.PhotoUrl + "/" + newName,
				Width: m.Rect.Max.X,
				Height: m.Rect.Max.Y,
			},
			Thumbnail: PhotoSize{
				Link: config.PhotoUrl + "/thumbnails/" + newName,
				Width: thumb.Rect.Max.X,
				Height: thumb.Rect.Max.Y,
			},
			Date: time.Now(),
		}

		f, err := json.MarshalIndent(photo, "", " ")
			if err != nil {
				log.Fatal(err)
		}

		err = ioutil.WriteFile(filepath.Join(config.PhotoDirectory, "/", strings.Replace(newName, ".jpg", ".json", 1)), f, 0644)

		if err != nil {
			log.Fatal(err)
		}


		return newName, buf, thumbBuf
}

func ListPhotos(config PhotoConfig) []Photo {
	var allPhotos []Photo
	allPaths, err := ioutil.ReadDir(config.PhotoDirectory)
	if err != nil {
		log.Fatal( "Can't read photo directory.")
	}

	for _, filename := range allPaths {
		name := filepath.Join(config.PhotoDirectory, filename.Name())
		var photo Photo
		file, err := ioutil.ReadFile(name)
		if err != nil {
			log.Fatal( "Can't read photo.")
		}
		err = json.Unmarshal(file, &photo)

		if err != nil {
			log.Fatal( "Can't parse photo json.")
		}

		allPhotos = append(allPhotos, photo)
	}
	return allPhotos
}

func DeleteLocal(name string, config PhotoConfig) error {
	filename := strings.Replace(name, ".jpg", ".json", 1)

	err := os.Remove(filepath.Join(config.PhotoDirectory +"/" +filename))

	if err != nil {
		return err
	}

	return nil
}

func GenerateSummary(dataDir string, config PhotoConfig) error {
	photos := ListPhotos(config)

	file, err := json.MarshalIndent(photos, "", " ")
	if err != nil {
		return err
	}

	filename := filepath.Join(dataDir, "/photos.json")

	err = ioutil.WriteFile(filename, file, 0644)

	if err != nil {
		return err
	}

	return nil
}

func adjustFilename(name string) string {
	n := strings.Split(name, ".")[0]
	s := slug.Make(n)
	return s + ".jpg"
}

