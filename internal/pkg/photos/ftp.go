package photos

import (
	"bytes"
	"github.com/jlaffaye/ftp"
	"io"
	"log"
	"time"
)


func getConnection(config PhotoConfig) *ftp.ServerConn {
	c, err := ftp.Dial(config.Host + ":21", ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	err = c.Login(config.Username, config.Password)
	if err != nil {
		log.Fatal(err)
	}

	return c
}

// func ListRemoteFiles(config PhotoConfig) []File {
// 	c := getConnection(config)
// 	err:= c.ChangeDir(config.RemotePath)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	entries, err := c.List(".")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	sort.Sort(ByTime(entries))

// 	var photos []Photo

// 	for _, entry := range entries {
// 		if strings.HasSuffix(entry.Name, ".JPG") || strings.HasSuffix(entry.Name, ".jpg") {
// 			photo := Photo{
// 				Name: entry.Name,
// 				Full: PhotoSize{
// 					Link: config.PhotoUrl + "/" + entry.Name,
// 					Width: 0,
// 					Height: 0,
// 				},
// 				Thumbnail: PhotoSize{
// 					Link: config.PhotoUrl + "/thumbnails/" + entry.Name,
// 					Width: 0,
// 					Height: 0,
// 				},
// 				Date: entry.Time,
// 			}

// 			photos = append(photos, photo)
// 		}
// 	}

// 	err = c.Quit()

// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	return photos
// }

type TempFile struct {
	Filename string
	Data io.Reader
}

func UploadPhotos(config PhotoConfig, files []TempFile, filename string) {
	c := getConnection(config)
	err:= c.ChangeDir(config.RemotePath)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		name, full, thumbnail := createPhoto(file, config)

		err = c.Stor(name, bytes.NewReader(full.Bytes()))
		if err != nil {
			log.Fatal(err)
		}

		err = c.Stor("thumbnails/" + name, bytes.NewReader(thumbnail.Bytes()))
		if err != nil {
			log.Fatal(err)
		}
	}

	err = c.Quit()

	if err != nil {
		log.Fatal(err)
	}
}

func DeletePhoto(filename string, config PhotoConfig) error {
	c := getConnection(config)
	err:= c.ChangeDir(config.RemotePath)
	if err != nil {
		return err
	}

	err = c.Delete(filename)

	if err != nil {
		return err
	}

	err = c.Delete("thumbnails/" + filename)

	if err != nil {
		return err
	}

	err = DeleteLocal(filename, config)

	if err != nil {
		return err
	}

	err = c.Quit()

	if err != nil {
		return err
	}

	return nil
}

// HELPER

type ByTime []*ftp.Entry

func (t ByTime) Len() int {
	return len(t)
}

func (t ByTime) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t ByTime) Less(i, j int) bool {
	return t[i].Time.Before(t[j].Time)
}
