import axios from 'axios'

export const state = () => ({
  list: [],
  activeProject: {}
})

export const mutations = {
  SET_PROJECTS(state, projectList) {
    state.list = projectList
  },
  SET_ACTIVE(state, projectSlug) {
    let project = state.list.filter(project => {
      return project.slug == projectSlug
    })[0]

    state.activeProject = project
  },

  SET_ACTIVE_NEW(state, newProject) {
    state.activeProject = newProject
  },

  // PROJECT UPDATES

  updateTitle(state, value) {
    state.activeProject.title = value
  },
  updateLink(state, value) {
    state.activeProject.link = value
  },
  updateDirector(state, value) {
    state.activeProject.director = value
  },
  updateCamera(state, value) {
    state.activeProject.camera = value
  },
  updateProduction(state, value) {
    state.activeProject.production = value
  },
  updateYear(state, value) {
    state.activeProject.year = value
  },
  updateAdditionalJobs(state, value) {
    state.activeProject.additional_jobs = value
  },
  updateDescription(state, value) {
    state.activeProject.description = value
  },

  addPosition(state, value) {
    if (state.activeProject.positions === null) {
      state.activeProject.positions = []
    }
    state.activeProject.positions.push(value)
  },

  removePosition(state, value) {
    state.activeProject.positions.splice(
      state.activeProject.positions.indexOf(value),
      1
    )
  },
  updateDisplayInProjects(state, value) {
    state.activeProject.display_in_projects = value
  },
  updateIsStickied(state, value) {
    state.activeProject.is_stickied = value
  }
}

export const getters = {
  sortedProjects(state) {
    let projects = [...state.list].sort((a, b) => {
      if (a.timestamp > b.timestamp) return -1
      if (a.timestamp < b.timestamp) return 1
      return 0
    })

    return projects
  },

  positions(state) {
    if (state.activeProject.positions === null) {
      return []
    }
    return state.activeProject.positions
  }
}

export const actions = {
  async GET_PROJECTS({ commit }) {
    const { data } = await axios.get('http://localhost:8080/api/projects')
    commit('SET_PROJECTS', data)
  },
  async DELETE_PROJECT({ dispatch }, project) {
    const { data } = await axios.delete(
      'http://localhost:8080/api/projects/' + project.slug
    )
    dispatch('GET_PROJECTS')
  },
  async UPDATE_PROJECT({ dispatch, state }) {
    const { data } = await axios.patch(
      'http://localhost:8080/api/projects/' + state.activeProject.slug,
      state.activeProject
    )
    dispatch('positions/UPDATE_POSITIONS', state.activeProject.positions, {
      root: true
    })
    await dispatch('GET_PROJECTS')
    return data.slug
  },
  async NEW_PROJECT({ commit }) {
    const { data } = await axios.get('http://localhost:8080/api/new-project')
    commit('SET_ACTIVE_NEW', data)
  },
  async CREATE_PROJECT({ dispatch, state }) {
    const { data } = await axios.post(
      'http://localhost:8080/api/projects',
      state.activeProject,
      {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    )
    dispatch('positions/UPDATE_POSITIONS', state.activeProject.position, {
      root: true
    })
    await dispatch('GET_PROJECTS')
    return data.slug
  },
  async GENERATE() {
    const res = await axios.post('http://localhost:8080/api/generate')
    if (res.status == 200) {
      return true
    }

    return false
  }
}
