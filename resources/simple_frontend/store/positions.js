import axios from 'axios'

export const state = () => ({
  list: []
})

export const mutations = {
  SET_POSITIONS(state, positionList) {
    state.list = positionList
  },
  UPDATE_POSITIONS(state, positions) {
    for (let position of positions) {
      let newPosition = { title: position }

      var found = state.list.find(elem => {
        return elem.title == position
      })

      if (!found) {
        state.list.push(newPosition)
      }
    }
  }
}

export const actions = {
  async GET_POSITIONS({ commit }) {
    const { data } = await axios.get('http://localhost:8080/api/positions')
    commit('SET_POSITIONS', data)
  },
  UPDATE_POSITIONS({ commit }, position) {
    commit('UPDATE_POSITIONS', position)
  }
}
