package server

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/jshsj/simple_cms/internal/pkg/projects"
	"gitlab.com/jshsj/simple_cms/internal/pkg/remote"
	"gitlab.com/jshsj/simple_cms/internal/pkg/photos"
)

type ServerError struct {
	Message string
}

func (e *ServerError) Error() string {
	return fmt.Sprintf("%s", e.Message)
}

type CustomContext struct {
	echo.Context
	dataDir string
	projectConfig projects.ProjectConfig
	gitConfig     remote.GitConfig
	photoConfig     photos.PhotoConfig
}

func StartServer(dataDir string, pconfig projects.ProjectConfig, gconfig remote.GitConfig, fconfig photos.PhotoConfig) {
	e := echo.New()

	e.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &CustomContext{
				c,
				dataDir,
				pconfig,
				gconfig,
				fconfig,
			}
			return h(cc)
		}
	})

	e.Use(middleware.CORS())
	e.Static("/", "resources/simple_frontend/dist")

	e.GET("/api/projects", listProjects)
	e.GET("/api/new-project", newProject)
	e.POST("/api/projects", createProject)
	e.GET("/api/projects/:slug", getProject)
	e.DELETE("/api/projects/:slug", deleteProject)
	e.PATCH("/api/projects/:slug", updateProject)

	e.GET("/api/positions", listPositions)
	e.POST("/api/generate", generateSummaries)

	e.GET("/api/photos", listPhotos)
	e.POST("/api/photos", uploadPhoto)
	e.DELETE("/api/photos/:slug", deletePhoto)

	e.Logger.Fatal(e.Start(":8080"))
}