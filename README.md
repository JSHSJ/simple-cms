# Simple CMS

A proof of concept / fully working "offline" CMS for managing content for a portfolio website.

### Goals

1. I wanted a static portfolio website
2. I didn't want a CMS running on a server, which is used maybe 0.5% of the uptime
3. I didn't want the person managing the files to be concerned with markdown / git / whatever else. It had to be simple and intuitive
4. (optional) I wanted it designed to be pleasing to use

### Process

Instead of going into details in a README I'll write a longer [blog post](https://joshuastuebner.com/blog/backend/simple_cms.html) about the details and link it here.

However, this diagram should give a good overview about the setup. That might just suffice for most.

![Simple CMS Diagram](/simple_cms.png "Simple CMS Diagram")

### Notes

I would **strongly** advice not to clone this repo and use it for your own, as it has many things hard coded,
or designed a specific way, that only makes sense in this project itself.

It is, however, open source. So use it, if you want to.

To start, either build or run `main.go` in `cmd/simple_cms/`.
To edit the frontend, navigate to `resources/simple_frontend`, run `npm install`, then `npm run dev`.

### Changelog

- Now supports photo upload using FTP
