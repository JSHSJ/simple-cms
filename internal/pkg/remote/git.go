package remote

import (
	"fmt"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	"log"
	web "net/http"
	"net/url"
	"os"
	"time"
)

type GitConfig struct {
	LocalDirectory string
	RemoteURL      string
	Username       string
	Password       string
	PipelineURL    string
	PipelineToken  string
	PipelineRef    string
}

type GitError struct {
	Message string
}

func (e *GitError) Error() string {
	return fmt.Sprintf("%s", e.Message)
}

func SetupProjects(config GitConfig) error {
	_, err := git.PlainClone(config.LocalDirectory, false, &git.CloneOptions{
		Auth: &http.BasicAuth{
			Username: config.Username,
			Password: config.Password,
		},
		URL:      config.RemoteURL,
		Progress: os.Stdout,
	})

	if err != nil {
		return &GitError{Message: "Can't clone projects."}
	}

	return nil
}

func FetchProjects(config GitConfig) error {
	r, err := git.PlainOpen(config.LocalDirectory)

	if err != nil {
		return &GitError{Message: "Can't open projects."}
	}

	// Get the working directory for the repository
	w, err := r.Worktree()

	if err != nil {
		return &GitError{Message: "Can't open worktree."}
	}

	_ = w.Pull(&git.PullOptions{
		RemoteName: "origin",
		Auth: &http.BasicAuth{
			Username: config.Username,
			Password: config.Password,
		}})

	return nil
}

func PushProjects(config GitConfig) error {
	// Opens an already existing repository.
	r, err := git.PlainOpen(config.LocalDirectory)

	if err != nil {
		return &GitError{Message: "Can't open projects."}
	}

	w, err := r.Worktree()

	if err != nil {
		return &GitError{Message: "Can't open worktree."}
	}

	_, err = w.Add(".")

	if err != nil {
		return &GitError{Message: "No data added - No need to push."}
	}

	dt := time.Now()

	_, err = w.Commit("CMS Push: "+dt.String(), &git.CommitOptions{
		Author: &object.Signature{
			Name:  "GG BOT",
			Email: "kontakt@joshuastuebner.com",
			When:  time.Now(),
		},
	})

	if err != nil {
		return &GitError{Message: "Can't commit data."}
	}

	err = r.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: config.Username,
			Password: config.Password,
		},
	})

	if err != nil {
		log.Fatal(err)
		return &GitError{Message: "Can't push data."}
	}

	return nil
}

func TriggerRebuild(config GitConfig) error {
	_, err := web.PostForm(
		config.PipelineURL,
		url.Values{
			"token": {config.PipelineToken},
			"ref":   {config.PipelineRef}})

	if err != nil {
		log.Fatal(err)
	}

	return nil
}
