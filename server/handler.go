package server

import (
	"github.com/labstack/echo"
	"gitlab.com/jshsj/simple_cms/internal/pkg/projects"
	"gitlab.com/jshsj/simple_cms/internal/pkg/photos"
	"net/http"
)

func listProjects(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	ps, err := projects.ListProjects(cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, ps)
}

func getProject(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	slug := c.Param("slug")
	p, err := projects.GetProject(slug, cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, p)
}

func newProject(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	p := projects.NewProject()

	return c.JSON(http.StatusOK, p)
}

func updateProject(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	slug := c.Param("slug")

	p := new(projects.ProjectData)

	if err := c.Bind(p); err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	newSlug, err := projects.UpdateProject(slug, *p, cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	resp := map[string]string{
		"slug": newSlug,
	}

	return c.JSON(http.StatusOK, resp)
}

func createProject(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	p := new(projects.ProjectData)

	if err := c.Bind(p); err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	newSlug, err := projects.SaveProject(*p, cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	resp := map[string]string{
		"slug": newSlug,
	}

	return c.JSON(http.StatusOK, resp)
}

func deleteProject(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	slug := c.Param("slug")
	err := projects.DeleteProject(slug, cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	return c.NoContent(http.StatusOK)
}

func listPositions(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	rs, _ := projects.ListPositions(cc.projectConfig)

	return c.JSON(http.StatusOK, rs)
}

func generateSummaries(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	err := projects.GenerateSummaries(cc.projectConfig)

	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	err = photos.GenerateSummary(cc.dataDir, cc.photoConfig)

	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	err = remote.PushProjects(cc.gitConfig)
	
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	err = remote.TriggerRebuild(cc.gitConfig)

	return c.NoContent(http.StatusOK)
}

// PHOTOS

func listPhotos(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	photos := photos.ListPhotos(cc.photoConfig)

	return c.JSON(http.StatusOK, photos)
}

func uploadPhoto(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	form, err := c.MultipartForm()
	if err != nil {
		return err
	}

	files := form.File

	var tempFiles []photos.TempFile

	for _, file := range files {

		src, err := file[0].Open()
		if err != nil {
			return err
		}
		defer src.Close()
		tmpFile := photos.TempFile{
			Filename: file[0].Filename,
			Data:     src,
		}
		tempFiles = append(tempFiles, tmpFile)
	}

	photos.UploadPhotos(cc.photoConfig, tempFiles, "test")

	return c.NoContent(http.StatusOK)
}

func deletePhoto(c echo.Context) error {
	cc := c.(*CustomContext)
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	slug := c.Param("slug")

	err := photos.DeletePhoto(slug, cc.photoConfig)

	if err != nil {
		return c.NoContent(http.StatusNotFound)
	}

	return c.NoContent(http.StatusOK)
}
