import axios from "axios";

export const state = () => ({
    list: [],
    loading: false
})

export const mutations = {
    SET_PHOTOS (state, photoList) {
        state.list = photoList
        state.loading = false
    },
    SET_LOADING(state, isLoading) {
        state.loading = isLoading
    }
}

export const actions = {
    async GET_PHOTOS({commit}) {
        commit("SET_LOADING", true)
        const { data } = await axios.get("http://localhost:8080/api/photos")
        commit('SET_PHOTOS', data)
    },
    async DELETE({dispatch}, filename) {
        const { data } = await axios.delete("http://localhost:8080/api/photos/" + filename)
        dispatch("GET_PHOTOS")
    },
    async UPLOAD({dispatch}, photos) {
        let formData = new FormData();

        photos.forEach(photo => {
            formData.append(photo.name, photo)
        })

        const { data } = await axios.post("http://localhost:8080/api/photos", 
        formData,
        { headers: { "Content-Type": "multiplart/form-data"}})
        dispatch("GET_PHOTOS")
    }
}