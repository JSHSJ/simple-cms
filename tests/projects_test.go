package tests

import (
	"github.com/gosimple/slug"
	"github.com/stretchr/testify/assert"
	"gitlab.com/jshsj/simple_cms/internal/pkg/projects"
	"testing"
)

func TestNewProject(t *testing.T) {
	newProject := projects.NewProject()

	emptyProject := projects.ProjectData{
		Title:          "",
		Slug:           "",
		Link:           "",
		Description:    "",
		Director:       "",
		Production:     "",
		Camera:         "",
		Year:           0,
		AdditionalJobs: "",
		Position:       "",
	}

	assert.Equal(t, newProject, emptyProject)
}

func TestSaveProject(t *testing.T) {
	testData := projects.ProjectData{
		Title:          "Test",
		Slug:           "",
		Link:           "http://test.test",
		Description:    "A testing test",
		Director:       "Test",
		Camera:         "Test",
		Production:     "Test",
		Year:           2019,
		AdditionalJobs: "Tolle Sachen",
		Position:       "Test",
	}

	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	newSlug, err := projects.SaveProject(testData, testConfig)
	assert.NoError(t, err)

	filename := slug.Make(testData.Title) + ".json"

	assert.Equal(t, slug.Make(testData.Title), newSlug)
	assert.FileExists(t, testConfig.ProjectDirectory+filename)
}

func TestUpdateProject(t *testing.T) {
	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	oldSlug := "test"

	testData := projects.ProjectData{
		Title:          "Test-Zwei",
		Slug:           "",
		Link:           "http://test-zwei.test",
		Description:    "A testing test",
		Director:       "Test",
		Camera:         "Test",
		Production:     "Test",
		Year:           2019,
		AdditionalJobs: "Tolle Sachen",
		Position:       "Test",
	}

	newSlug, err := projects.UpdateProject(oldSlug, testData, testConfig)

	assert.NoError(t, err)

	filename := slug.Make(testData.Title) + ".json"

	assert.Equal(t, slug.Make(testData.Title), newSlug)
	assert.FileExists(t, testConfig.ProjectDirectory+filename)
}

func TestGetProject(t *testing.T) {
	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	project, err := projects.GetProject("Test-Zwei", testConfig)
	assert.NoError(t, err)
	assert.Equal(t, project.Title, "Test-Zwei")
	assert.Equal(t, project.Year, 2019)
	assert.Equal(t, project.Slug, "test-zwei")
}

func TestListProjects(t *testing.T) {
	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	ps, err := projects.ListProjects(testConfig)
	assert.NoError(t, err)
	assert.Equal(t, len(ps), 1)

	project := ps[0]
	assert.Equal(t, project.Title, "Test-Zwei")
	assert.Equal(t, project.Year, 2019)
	assert.Equal(t, project.Slug, "test-zwei")
}

func TestDeleteProject(t *testing.T) {
	testData := projects.ProjectData{
		Title:          "Delete me",
		Slug:           "",
		Link:           "http://test.test",
		Description:    "A testing test",
		Director:       "Test",
		Camera:         "Test",
		Production:     "Test",
		Year:           2019,
		AdditionalJobs: "Tolle Sachen",
		Position:       "Test",
	}

	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	newSlug, err := projects.SaveProject(testData, testConfig)
	assert.NoError(t, err)

	err = projects.DeleteProject(newSlug, testConfig)
	assert.NoError(t, err)
}

func TestGenerateSummaries(t *testing.T) {

	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	err := projects.GenerateSummaries(testConfig)
	assert.NoError(t, err)
}

func TestListPositions(t *testing.T) {
	testConfig := projects.ProjectConfig{
		ProjectDirectory: "./testdata/projects/",
		PositionsFile:    "./testdata/positions.json",
		ProjectsFile:     "./testdata/projects.json",
	}

	positions, err := projects.ListPositions(testConfig)
	assert.NoError(t, err)
	assert.Equal(t, len(positions.Positions), 1)

	position := positions.Positions[0]
	assert.Equal(t, position.Title, "Test")
}
