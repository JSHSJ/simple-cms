package projects

import (
	"encoding/json"
	"fmt"
	"github.com/gosimple/slug"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

type ProjectError struct {
	Message string
}

func (e *ProjectError) Error() string {
	return fmt.Sprintf("%s", e.Message)
}

type ProjectConfig struct {
	ProjectDirectory string
	PositionsFile    string
	ProjectsFile     string
}

type ProjectData struct {
	Title          string    `json:"title"`
	Slug           string    `json:"slug"`
	Link           string    `json:"link"`
	Description    string    `json:"description"`
	Director       string    `json:"director"`
	Production     string    `json:"production"`
	Camera         string    `json:"camera"`
	Year           int       `json:"year"`
	AdditionalJobs string    `json:"additional_jobs"`
	Positions       []string    `json:"positions"`
	DisplayInProjects bool `json:"display_in_projects"`
	IsStickied bool `json:"is_stickied"`
	Timestamp      time.Time `json:"timestamp"`
}

func ListProjects(config ProjectConfig) ([]ProjectData, error) {
	var allProjects []ProjectData
	allPaths, err := ioutil.ReadDir(config.ProjectDirectory)
	if err != nil {
		return allProjects, &ProjectError{Message: "Can't read project directory."}
	}

	for _, filename := range allPaths {
		name := filepath.Join(config.ProjectDirectory, filename.Name())
		var project ProjectData
		file, err := ioutil.ReadFile(name)
		if err != nil {
			return allProjects, &ProjectError{Message: "Can't read file: " + name}
		}
		err = json.Unmarshal(file, &project)

		if err != nil {
			return allProjects, &ProjectError{Message: "Can't parse json: " + name}
		}

		allProjects = append(allProjects, project)
	}
	return allProjects, nil
}

func NewProject() ProjectData {
	var emptySlice []string

	return ProjectData{
		Title:          "",
		Slug:           "",
		Link:           "",
		Description:    "",
		Director:       "",
		Camera:         "",
		Production:     "",
		Year:           0,
		AdditionalJobs: "",
		Positions:       emptySlice,
		DisplayInProjects: true,
		IsStickied: 		false,
		Timestamp:      time.Time{},
	}
}

func UpdateProject(oldSlug string, projectData ProjectData, config ProjectConfig) (string, error) {
	projectData.Slug = slug.Make(projectData.Title)

	err := DeleteProject(oldSlug, config)

	if err != nil {
		return "", &ProjectError{Message: "Project not found."}
	}

	newSlug, err := SaveProject(projectData, config)

	if err != nil {
		return "", &ProjectError{Message: "Can't update Project"}
	}

	return newSlug, nil
}

func SaveProject(projectData ProjectData, config ProjectConfig) (string, error) {
	if projectData.Slug == "" {
		projectData.Slug = slug.Make(projectData.Title)
	} else if !slug.IsSlug(projectData.Slug) {
		return "", &ProjectError{Message: "Invalid slug."}
	}

	if projectData.Timestamp.IsZero() {
		projectData.Timestamp = time.Now()
	}

	file, err := json.MarshalIndent(projectData, "", " ")
	if err != nil {
		return "", &ProjectError{Message: "Can't create json."}
	}
	filename := makeFileName(projectData.Title)
	err = ioutil.WriteFile(filepath.Join(config.ProjectDirectory+filename), file, 0644)

	if err != nil {
		return "", &ProjectError{Message: "Can't write file: " + filepath.Join(config.ProjectDirectory+filename)}
	}

	return projectData.Slug, nil
}

func GetProject(name string, config ProjectConfig) (ProjectData, error) {
	filename := makeFileName(name)
	file, err := ioutil.ReadFile(filepath.Join(config.ProjectDirectory + filename))
	if err != nil {
		return ProjectData{}, &ProjectError{Message: "Can't read file: " + filepath.Join(config.ProjectDirectory+filename)}
	}

	var project ProjectData
	err = json.Unmarshal(file, &project)

	if err != nil {
		return ProjectData{}, &ProjectError{Message: "Can't parse file: " + filepath.Join(config.ProjectDirectory+filename)}
	}

	return project, nil
}

func DeleteProject(name string, config ProjectConfig) error {
	filename := makeFileName(name)

	err := os.Remove(filepath.Join(config.ProjectDirectory + filename))

	if err != nil {
		return &ProjectError{Message: "Can't write file: " + filepath.Join(config.ProjectDirectory+filename)}
	}

	return nil
}

func GenerateSummaries(config ProjectConfig) error {
	allProjects, err := ListProjects(config)

	if err != nil {
		return &ProjectError{Message: "Can't get projects."}
	}

	file, err := json.MarshalIndent(allProjects, "", " ")
	if err != nil {
		return &ProjectError{Message: "Can't create json."}
	}

	err = ioutil.WriteFile(config.ProjectsFile, file, 0644)

	allPositions := []PositionData{}

	for _, project := range allProjects {
		for _, p := range project.Positions {
		position :=	PositionData{Title: p}

		if len(allPositions) == 0 {
			allPositions = append(allPositions, position)
		} else {
			seen := false
			for i := range allPositions {
				if allPositions[i] == position {
					seen = true
				}
			}
			if !seen {
				allPositions = append(allPositions, position)
			}
		}
	}
	}

	file, err = json.MarshalIndent(allPositions, "", " ")
	if err != nil {
		return &ProjectError{Message: "Can't create json."}
	}
	err = ioutil.WriteFile(config.PositionsFile, file, 0644)

	return err
}

func makeFileName(name string) string {
	newSlug := slug.Make(name)
	return "/" + newSlug + ".json"
}
