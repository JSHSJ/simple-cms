
export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: '181 Management',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/apple-touch-icon.png', sizes: "180x180" },
      { rel: 'shortcut icon', type: 'image/png', href: '/favicon-32x32.png', sizes: "32x32" },
      { rel: 'shortcut icon', type: 'image/png', href: '/favicon-16x16.png', sizes: "16x16" },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'stylesheet', href: "https://fonts.googleapis.com/css?family=Montserrat:300,400,600&display=swap"}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "@/plugins/store-init.js"
  ],
  /*
  ** Nuxt.js dev-modules
  */
  devModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },

  styleResources: {
    // your settings here
    sass: [
      "assets/vars/*.scss"
    ], // alternative: scss
   },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
